<?php

namespace CityScape;

/**
 * @class Generator
 * CityScape\Generator handles the main generation functionality when the main form
 * is submitted or we have parameters to parse. If we do not have any parameters or
 * are trying to help the views, use CityScape\Library instead. This is only to build
 * out a class based on the user's chosen attributes in our frontend form. This is also
 * used to respond to AJAX requests. In that case we simply build out our generator, and
 * respond with the file path of the generated image.
 */
class Generator {
    /**
     * @var $lookups array
     * the array of the parsed configuration file
     */
    private $lookups;

    /**
     * @var $trim array
     * array of form values parsed for the trim
     */
    private $trim;

    /**
     * @var $body array
     * array of form values parsed for the body
     */
    private $body;

    /**
     * @var $filePath string
     * The parsed file path based on the body name, color, trim name, & color.
     * we parse every form the same way and store the files in a consistent manner.
     * therefore it's easy to determine the file's path.
     */
    private $filePath;

    /**
     * @var $graphic boolean
     * A boolean that is for whether or not the chosen panel style/color is graphic or custom.
     * If it is we do a very specific set of tasks based on that and don't generate images.
     * There is a /custom directory that we just pull the image from.
     */
    private $graphic;

    /**
     * @var $serverFilePath string
     * The prefix to be added to file paths. This is needed for when switching server environments
     * and working locally.
     */
    private $serverFilePath;

    /**
     * @var $serverAssetPath string
     * The prefix to be added to asset paths. This is needed for when switching server environments
     * and working locally.
     */
    private $serverAssetPath;

    /**
     * @function __construct
     * @params form parameters that include styles & colors from the generator or saved image.
     * When we generate a new 'Generator' we set private class variables containing information
     * on the submitted form such as trim info, body info, and the file path. This in turn, after
     * setting all proper variables, processes the information and ends there.
     */
    function __construct($params) {
        $this->parseConfig();
        $trimStyle     = $params['trim_style'];
        $trimColor     = $params['trim_color'];

        $bodyStyle     = $params['body_style'];
        $bodyColor     = $params['body_color'];

        $this->trim = array(
            "color"       => $this->lookups['colors'][$trimColor],
            "color_name"  => $trimColor,
            "style"       => $this->lookups['trim'][$trimStyle],
            "style_param" => $trimStyle
        );

        $this->body = array(
            "color"       => $this->lookups['colors'][$bodyColor],
            "color_name"  => $bodyColor,
            "style"       => $this->lookups['body'][$bodyStyle],
            "style_param" => $bodyStyle
        );

        if ($this->debug()) {
            print_r($this->trim);
            print_r($this->body);
        }

        if ($this->lookups['settings']['process_image']) {
            $this->processImage();
        }
    }

    /**
     * @function getFilePath
     * Getter for $this->filePath
     * return string file path
     */
    function getFilePath() {
        return $this->filePath;
    }

    /**
     * @function processImage
     * Main handler for taking Generator variables, parsing them, submitting them to our
     * shell script and that's about it. Nothing needs to be returned since this is only concerned
     * with generating the image, not returning the path since we already know what the path will be
     * by using a naming convention.
     */
    function processImage() {
        $bcolor = $this->body['color'];
        $bname  = $this->body['style_param'];
        $tcolor = $this->trim['color'];
        $tname  = $this->trim['style_param'];

        if ($this->body['color_name'] == 'graphic') {
            $this->filePath = $this->serverAssetPath . "app/assets/images/custom/custom.png";
            $this->graphic  = TRUE;
        } else {
            $this->filePath = Library::filePath($bname, $bcolor, $tname, $tcolor);

            if (!file_exists($this->filePath)) {
                $cmd   = "/usr/bin/bash ./bin/process_image $bcolor $tcolor trim_$tname body_$bname";
                $image = shell_exec($cmd);

                if ($this->debug()) {
                    print_r($image);
                }
            }
        }
    }

    /**
     * @function parseConfig
     * Wrapper around Library::getLookup() that simply sets $this->lookups to that
     * configuration array.
     */
    function parseConfig() {
        $this->lookups   = Library::getLookup();
        $this->serverFilePath  = Library::getFilePath($this->lookups);
        $this->serverAssetPath = Library::getAssetPath($this->lookups);
    }

    /**
     * @function debug
     * Determine whether or not we are in 'debug' mode. If we are, certain things will
     * be printed out throughout the process. This is controlled by our .ini file.
     * @return t/f
     */
    function debug() {
        if ($this->lookups['settings']['debug']) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @function getPanelName
     * @param $parse boolean
     * Return either body style or style parameterized based on whether parse is t/f
     * @return string
     */
    function getPanelName($parse = true) {
        if ($this->graphic) {
            return 'Graphic';
        } else {
            return $parse ? $this->body['style'] : $this->body['style_param'];
        }
    }

    /**
     * @function getPanelImage
     * Get the path for the panel image for the results page
     * @return $string panel image path
     */
    function getPanelImage() {
        if ($this->graphic) {
            return $this->serverAssetPath . "app/assets/images/graphic.png";
        } else {
            $panel = $this->getPanelName(false);
            return $this->serverAssetPath . "app/assets/images/styles/$panel.png";
        }
    }

    /**
     * @function getTrimName
     * @param $parse boolean
     * Return either trim style or style parameterized based on whether parse is t/f
     * @return string
     */
    function getTrimName($parse = true) {
        return $parse ? $this->trim['style'] : $this->trim['style_param'];
    }

    /**
     * @function getTrimImage
     * Get the path for the trim image for the results page
     * @return $string trim image path
     */
    function getTrimImage() {
        $trim = $this->getTrimName(false);
        return $this->serverAssetPath . "app/assets/images/trims/$trim.png";
    }

    /**
     * @function getMaterial
     * return name of panel material (metal or Acrylicap)
     * @return string
     */
    function getMaterial() {
        if ($this->graphic) {
            return 'Graphic';
        } else {
            $name = $this->getPanelName();
            return $this->parseMaterial($name);
        }
    }

    /**
     * @function parseMaterial
     * return name of panel material (metal or Acrylicap)
     * @return string
     */
    function parseMaterial($name) {
        $split = explode('_', $name);
        $last = end($split);

        return $last == 'metal' ? 'Metal' : 'Acrylicap';
    }

    /**
     * @function getPanelColor
     * @param $parse boolean
     * Return either body color or color parameterized based on whether parse is t/f
     * @return string
     */
    function getPanelColor($parse = true) {
        $param = $this->body['color_name'];
        return $parse ? Library::parseColor($param) : $param;
    }

    /**
     * @function getTrimColor
     * @param $parse boolean
     * Return either trim color or color parameterized based on whether parse is t/f
     * @return string
     */
    function getTrimColor($parse = true) {
        $param = $this->trim['color_name'];
        return $parse ? Library::parseColor($param) : $param;
    }

    /**
     * @function generateShareLink
     * Generates the href value for our save and share links. This is where our Base64
     * encoded /?result= parameter comes from. Since we follow a strict naming convention,
     * all we really need to know is the Generator's values in order for the user to visit
     * this again or share with people. There is no sensitive information in the hash or on the
     * result page. If someone were to run the hash against base64_decode they will simply see
     * a generated result page with no information on it per se.
     */
    function generateShareLink() {
        $pname  = $this->getPanelName(false);
        $pcolor = $this->getPanelColor(false);
        $tname  = $this->getTrimName(false);
        $tcolor = $this->getTrimColor(false);

        return "/?result=" . base64_encode("$pname-$pcolor-$tname-$tcolor");
    }
}
