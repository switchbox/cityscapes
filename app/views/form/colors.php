<h2 class="text-center"><span class="brand-green">COLORS&nbsp;</span><small class="italic">Standard and Made to Order</small></h2>

<div class="color-container">
    <div class="row">
      <div class="col-xs-12">
        <!-- START FOREACH -->
        <?php $check = true; foreach($lookup['colors'] as $color => $code) { ?>
            <?php if ($color != 'graphic') { ?>
                <div class="color">
                    <label for="<?php echo "$context" . "_color" ?>">
                        <div class="color-box img-radio" data-color="#<?php echo $code ?>"></div>
                        <span class="color-name"><?php echo \CityScape\Library::parseColor($color); ?> </span>
                        <input name="<?php echo "$context" . "_color" ?>" type="radio" value="<?php echo $color ?>" <?php if ($check) echo 'checked'; ?> />
                        <?php $check = false; ?>
                    </label>
                </div>
            <?php } ?>
        <?php } ?>
        <!-- END FOREACH -->
        <!-- Check for custom graphic image -->
        <?php if ($graphic) { ?>
            <div class="color">
                <label for="<?php echo "$context" . "_color" ?>">
                    <div class="color-box img-radio color-box-graphic"></div>
                    <span class="color-name">Graphic</span>
                    <input name="<?php echo "$context" . "_color" ?>" type="radio" value="graphic" />
                </label>
            </div>
        <?php } ?>
        <!-- END custom graphic block -->
      </div>
    </div>
</div>
