<?php

/**
 * CityScape Generator (Builder) index page. This serves as a router for the application. In essence
 * the application is a single page app consisting of:
 *                                                    1. The generator form itself
 *                                                    2. The results page with share links after POST
 *                                                    3. The results page based on a /?result hash.
 * When the user POSTs the form it will present them the results page with share links. The same
 * view is used when a user clicks the share link or shares that link. The GET version of the results
 * page uses base64 encoding to simply pass the body style, body color, trim style, and trim color as
 * a dash separated string. Since sensitive information is contained in the results page we are not
 * concerned about it being a simple b64 hash.
 */
include('app/lib/library.php');
include('app/lib/generator.php');

$lookup      = \CityScape\Library::getLookup();
$filePath    = \CityScape\Library::getFilePath($lookup);
$assetPath   = \CityScape\Library::getAssetPath($lookup);
$previewPath = \CityScape\Library::getPreviewPath($lookup);

/* When GET request and /?result param is not set */
if (\CityScape\Library::get() && empty($_GET['result'])) {
    /* set lookup config file for views */
    $path = $filePath . 'app/views/generator.php';
    include($filePath . 'app/views/wrapper.php');
    /* When POST request or if GET request with /?result param is not empty. */
} elseif (\CityScape\Library::post() || (\CityScape\Library::get() && !empty($_GET['result']))) {
    /**
     * If the POST[data] attribute is not empty we are expecting an AJAX request from a radio button
     * being changed on the front end. Here we don't care about a results page, we simply need to parse
     * the form and submit the file path back to our JS.
     */
    if (!empty($_POST['data'])) {
        $params = array();
        parse_str($_POST['data'], $params);
        $gen = new \CityScape\Generator($params);
        echo $gen->getFilePath();
    } else {
        /**
         * Here we are expecting either a GET request with the GET[result] attribute present or
         * a POST request which would mean the user is submitting the SAVE and SHARE form after
         * using the generator. Either way we want to simply grab the form params and ship that to
         * the Generator class's constructor. Library::formatParams is used if we are expecting a
         * /?result param b64 hash.
         */
        $params = empty($_GET['result']) ? $_POST : \CityScape\Library::formatParams($_GET['result']);
        $gen = new \CityScape\Generator($params);
        $path = $filepath . 'app/views/results.php';
        include($filePath . 'app/views/wrapper.php');
    }
} else {
    /* Only accept GET & POST requests. */
    die('Invalid Request.');
}
