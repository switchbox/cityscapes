<?php

namespace CityScape;

/**
 * @class Library
 * Main library for CityScape Generator (Builder). This library contains agnostic functions
 * that don't care about the Generator's form variables or state. This is meant to be used as
 * mainly public static functions that help out our front end.
 */
class Library {
    /**
     * @function get
     * Determine whether or not current request is GET request.
     * @return t/f
     */
    public static function get() {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @function post
     * Determine whether or not current request is POST request.
     * @return t/f
     */
    public static function post() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @function requestAction
     * return the PHP_SELF route in order for the form to send back to itself instead
     * of another PHP file somewhere else.
     * @return string
     */
    public static function requestAction() {
        return htmlspecialchars($_SERVER['PHP_SELF']);
    }

    /**
     * @function previewImage
     * return the first final image that's generated inside the app/assets/images/preview directory.
     * This is used primarily as a stub in order to display the preview image on the generator form.
     * @return string file path
     */
    public static function previewImage($prefix) {
        $path = "app/assets/images/preview";
        $finalDir = scandir($path);
        $files    = array();

        foreach($finalDir as $file) {
            $split = explode('.', $file);
            if ($split[1] == "png") {
                return ".//$path/$file";
            }
        }
    }

    /**
     * @function filePath
     * @param $bname body name
     * @param $bcolor body color
     * @param $tname trim name
     * @param $tname trim color
     * Returns the proper file path for a given set of generator parameters. Whenever
     * an image is generated it has a specific naming convention that enables us to find the
     * image based on the generation's attributes (name, color, etc.). This also enables us
     * to use this same standard for our GET request /?result parameters.
     * @return string file path
     */
    public static function filePath($bname, $bcolor, $tname, $tcolor) {
        return "./bin/finals/full-body_$bname-$bcolor-trim_$tname-$tcolor.png";
    }

    /**
     * @function getLookup
     * Parse lookups INI configuration file as an array.
     * @return array INI file configuration
     */
    public static function getLookup() {
        return parse_ini_file('app/config/lookups.ini');
    }

    /**
     * @function getColor
     * @param $lookup array configuration ini array
     * @param $key string which key to lookup inside the $lookup variable
     * Return the value for given key in configuration's color array.
     * @return string $lookup[$key] value
     */
    public static function getColor($lookup, $key) {
        return $lookup['colors'][$key];
    }

    /**
     * @function parseColor
     * @param $str undercased color string
     * Explode _ , join with space, uppercase all words. This is used for presenting color names
     * on the frontend where we only have access to hex codes and parameterized strings.
     * @return string formatted color name
     */
    public static function parseColor($str) {
        return ucwords(implode(' ', (explode('_', $str))));
    }

    /**
     * @function formatParams
     * @param $params base64 encoded string of generator parameters
     * Take a Base64 encoded string of parameters and decode them, split them, and package
     * them as natural form parameters in order to ship off and run the Generator constructor. This
     * is used as a middle man when the user is trying to perform a GET request with a completed
     * generator's hash set in /?result=[hash].
     * @return array generator parameters that can be sent to Generator::construct.
     */
    public static function formatParams($params) {
        $results = base64_decode($params);
        $ar      = explode('-', $results);

        return array(
            "body_style" => $ar[0],
            "body_color" => $ar[1],
            "trim_style" => $ar[2],
            "trim_color" => $ar[3]
        );
    }

    /**
     * @function getFilePath
     * @param $lookup array configuration array
     * Look up the current environment's file prefix path
     */
    public static function getFilePath($lookup) {
        $env = $lookup['environment']['current'];
        return $lookup[$env]['file_prefix'];
    }

    /**
     * @function getAssetPath
     * @param $lookup array configuration array
     * Look up the current environment's asset prefix path
     */
    public static function getAssetPath($lookup) {
        $env = $lookup['environment']['current'];
        return $lookup[$env]['asset_prefix'];
    }

    public static function getPreviewPath($lookup) {
        $env = $lookup['environment']['current'];
        return $lookup[$env]['preview_prefix'];
    }
}
