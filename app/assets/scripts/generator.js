$(function() {
    $.each($('.color-box'), function(key, value) {
        $(this).css('background-color', $(this).data('color'));
    });

    $("input").change(function() {
        if ($(this).val() == 'graphic') {
            $('img.preview').prop('src', './app/assets/images/custom/custom.png');
        } else {
            changePreviewImage();
        }
    });

    $('.img-radio').click(function() {
        $(this).siblings('input').click();
    });
});

function changePreviewImage() {
    $.post('index.php', { data: $('form').serialize() }).done(function(data) {
        $('img.preview').prop('src', data);
    });
}
