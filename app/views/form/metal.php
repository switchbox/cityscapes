  <div class="col-xs-4">
    <h4>Metal</h4>
    <div class="cap">
        <label for="body_style">
            <img class="img-radio" alt="72_rib_metal" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/72_rib_metal.png" height="70px" />
            <span class="body-name">72 Rib Metal</span><br>
            <input name="body_style" type="radio" value="72_rib_metal" />
        </label>
    </div>

    <div class="cap">
        <label for="body_style">
            <img class="img-radio" alt="pan_metal" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/pan_metal.png" height="70px" />
            <span class="body-name">Pan Metal</span><br>
            <input name="body_style" type="radio" value="pan_metal" />
        </label>
    </div>

    <div class="cap">
        <label for="body_style">
            <img class="img-radio" alt="vertical_rib_metal" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/vertical_rib_metal.png" height="70px" />
            <span class="body-name">Vertical Rib Metal</span><br>
            <input name="body_style" type="radio" value="vertical_rib_metal" />
        </label>
    </div>

    <div class="cap">
        <label for="body_style">
            <img class="img-radio" alt="perforated_metal" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/perforated_metal.png" height="70px" />
            <span class="body-name">Perforated Metal</span><br>
            <input name="body_style" type="radio" value="perforated_metal" />
        </label>
    </div>
  </div>
</div>
