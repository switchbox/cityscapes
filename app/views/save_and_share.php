<h1 class="text-center">
    <span class="brand-outline lg-half">STEP 3</span>
    <span class="brand-green light">SAVE and SHARE</span>
</h1>

<div class="row">
  <div class="col-xs-3">&nbsp;</div>
  <div class="col-xs-4">
    Share your design with your network, Save multiple drafts for future projects and, when you're ready, send to our project management team.
  </div>
  <div class="col-xs-4">
    <div class="save-button">
        <input name="btn" type="submit" value="SAVE AND SHARE" class="brand-green-background btn-share" />
    </div>
  </div>
  <div class="col-xs-1">&nbsp;</div>
</div>
