
<div class="row text-center">
    <div class="col-xs-12">
        <img alt="envisor_logo" src="<?php echo $assetPath . '/'; ?>app/assets/images/envisor_logo.png" />
        <div class="tagline">
            THE LEADING ROOF SCREEN CHOICE OF ARCHITECTS, BUILDING<br>
            OWNERS AND CONTRACTORS FOR MORE THAN 20 YEARS.
        </div>

        <h4 class="brand-green">DESIGN YOUR OWN</h4>
        <small>with our <span>CityScapes' Builder</span></small>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="">
            <img class="preview img-responsive center-block" alt="preview" src="<?php echo \CityScape\Library::previewImage($previewPath); ?>" />
        </div>
    </div>
</div>

<div class="row">
    <?php include($filePath . "app/views/progress_bar.php"); ?>
</div>

<div class="divider">&nbsp;</div>

<form action="<?php echo \CityScape\Library::requestAction(); ?>" method="post">
    <?php include($filePath . 'app/views/form/body.php'); ?>
    <?php
    $context = 'body';
    $graphic = TRUE;
    include($filePath . 'app/views/form/colors.php');
    ?>

    <div class="divider">&nbsp;</div>
    <?php include($filePath . 'app/views/form/trim.php'); ?>

    <?php
    $context = 'trim';
    $graphic = FALSE;
    include($filePath . 'app/views/form/colors.php');
    ?>

    <div class="divider">&nbsp;</div>
    <?php include($filePath . 'app/views/save_and_share.php'); ?>
</form>
