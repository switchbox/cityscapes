<?php

include('app/lib/library.php');
include('app/lib/generator.php');

$stagingRoot = "http://77.104.151.66/~covrit16/cityscapes_phase3/envisor-builder";
$resultHash  = $_GET['result'];
$redirectTarget = empty($resultHash) ?
                  $stagingRoot       :
                  $stagingRoot . '/?result=' . $resultHash;

if (
    strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||
    strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false
) {
    // it is probably Facebook's bot
    if (empty($resultHash)) {
        header("Location: $redirectTarget");
    } else {
        $params    = \CityScape\Library::formatParams($resultHash);
        $gen       = new \CityScape\Generator($params);
        $root      = 'http://77.104.151.66/~covrit16/cityscapes_phase3/';
        $image     = $root.'sb_envisor_builder/'.str_replace(['./','finals','.png'],['','shares','.jpg'],$gen->getFilePath());
        /* $imagePath = $gen->getFilePath();*/
    }
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Begin OG Tags -->

        <meta property="og:title" content="CityScape Builder - Share"/>
        <meta property="og:description" content="CityScape Builder - Share"/>
        <meta property="og:type" content="article"/>
        <meta property="article:section" content="CityScape"/>
        <meta property="og:url" content="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"/>
        <meta property="og:image" content="<?php echo $image; ?>"/>
        <meta property="og:image:width" content="652"/>
        <meta property="og:image:height" content="279"/>
        <meta property="og:image" content="<?php echo $image; ?>"/>
        <meta property="og:image:width" content="398"/>
        <meta property="og:image:height" content="207"/>
        <meta property="og:image" content="<?php echo $image; ?>"/>
        <meta property="og:image:width" content="207"/>
        <meta property="og:image:height" content="207"/>
        <meta property="og:site_name" content="CityScape Builder"/>
        <meta property="fb:app_id" content="135991653099218"/>

        <!-- End OG Tags -->

        <meta charset="UTF-8"/>
        <title>CityScapes - Share</title>
    </head>
    <body>
      <img src="<?php echo $image; ?>" />
    </body>
</html>

<?php

// end of if Facebook user agent bot
} else {
    // not Facebook's bot
    header("Location: $redirectTarget");
}
