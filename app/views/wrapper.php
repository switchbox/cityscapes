<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>CityScape Generator</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="<?php echo $assetPath . '/'; ?>app/assets/styles/generator.css" rel="stylesheet"/>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" id="st_insights_js" src="http://w.sharethis.com/button/buttons.js?publisher=75398ff8-5f3a-434d-bc45-2225d9d7558e"></script>
        <script type="text/javascript">stLight.options({publisher: "75398ff8-5f3a-434d-bc45-2225d9d7558e", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    </head>
    <body>
        <div class="container" style="width: 100%">
            <?php include($path); ?>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="<?php echo $assetPath . '/'; ?>app/assets/scripts/generator.js"></script>
    </body>
</html>
