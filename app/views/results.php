<div class="wrap_result" style="margin-top: 30px;">
<div class="row">
  <div class="col-xs-12">
    <img class="result_logo" alt="envisor_logo" src="<?php echo $assetPath . '/'; ?>app/assets/images/envisor_logo.png" class="img-responsive" /><br />
    <p class="envisor-text-results">
      THE LEADING ROOF SCREEN CHOICE OF ARCHITECTS, BUILDING<br>
      OWNERS AND CONTRACTORS FOR MORE THAN 20 YEARS.
    </p>
    <p class="envisor-text-sub brand-green-plain">
      ZERO ROOF PENETRATION | CODE-COMPLIANT | BRAND NAME COMPATIBLE | MADE TO ORDER | COST SAVINGS
    </p>
    <br /><br />
</div>
<div class="results-container">
    <div class="row">
      <div class="col-xs-4" style="margin-top: 44px;">
        <div class="row">
          <div class="col-xs-3">
            <img class="img-responsive" alt="<?php echo $gen->getPanelName(false); ?>" src="<?php echo $gen->getPanelImage(); ?>" />
          </div>
          <div class="col-xs-9">
            <p class="brand-green bold hi">PANEL STYLE and COLOR</p>
            <p>Panels: <?php echo $gen->getPanelName(); ?><br />
            Material: <?php echo $gen->getMaterial(); ?><br />
            Color: <?php echo $gen->getPanelColor(); ?></p>
          </div>
        </div>
        <br /><br />
        <div class="row">
          <div class="col-xs-3">
            <img class="img-responsive" alt="<?php echo $gen->getTrimName(false); ?>" src="<?php echo $gen->getTrimImage(); ?>" />
          </div>
          <div class="col-xs-9">
            <p class="brand-green bold hi">TRIM STYLE and COLOR</p>
            <p>Panels: <?php echo $gen->getTrimName(); ?><br />
            Material: Acrylicap<br />
            Color: <?php echo $gen->getTrimColor(); ?></p>
          </div>
        </div>
      </div>
      <div class="col-xs-8" style="margin-top: 44px;">
        <img class="preview-img img-responsive" alt="preview" src="<?php echo $gen->getFilePath(); ?>" style="position:relative; top: -50px;" />
        <div class="share-links text-right">
            <p><span class="brand-green-background btn-share">Share This Design</span></p>

            <?php
              $root = 'http://77.104.151.66/~covrit16/cityscapes_phase3/';
              $title = 'Envisor - DESIGN YOUR OWN with our CityScapes’ Builder.';
              $image = $root.'sb_envisor_builder/'.str_replace('./','',$gen->getFilePath());
              $summary = 'The leading roof screen choice of architects, building owners and contractors for more than 20 years.';
              $url = $root.'sb_envisor_builder/share.php'.$gen->generateShareLink();

            ?>

            <p>
              <span class='st_facebook_large' st_title='<?= $title ?>' st_image='<?= $image ?>' st_summary='<?= $summary ?>' st_url='<?= $url ?>' displayText='Facebook'></span>
              <span class='st_twitter_large' st_title='<?= $title ?>' st_image='<?= $image ?>' st_summary='<?= $summary ?>' st_url='<?= $url ?>' displayText='Tweet'></span>
              <span class='st_linkedin_large' st_title='<?= $title ?>' st_image='<?= $image ?>' st_summary='<?= $summary ?>' st_url='<?= $url ?>' displayText='LinkedIn'></span>
              <span class='st_pinterest_large' st_title='<?= $title ?>' st_image='<?= $image ?>' st_summary='<?= $summary ?>' st_url='<?= $url ?>' displayText='Pinterest'></span>
              <span class='st_email_large' st_title='<?= $title ?>' st_image='<?= $image ?>' st_summary='<?= $summary ?>' st_url='<?= $url ?>' displayText='Email'></span>
            </p>
        </div>
    </div>
  </div>
</div>
</div>
