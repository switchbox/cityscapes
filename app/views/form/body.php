<h1 class="text-center">
    <span class="brand-outline lg-half">STEP 1</span>
    <span class="brand-green light">PANEL STYLE and COLOR</span>
</h1>
<div class="row">
  <div class="col-xs-12">
    <?php include($filePath . 'app/views/form/acrylicap.php'); ?>
    <?php include($filePath . 'app/views/form/metal.php'); ?>
  </div>
</div>
