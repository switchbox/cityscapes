<h1 class="text-center">
    <span class="brand-outline lg-half">STEP 2</span>
    <span class="brand-green light">TRIM CAP STYLE and COLOR</span>
</h1>
<div class="row">
    <div class="col-sm-12">
        <!-- BEGIN FOREACH -->
        <?php $check = true; foreach($lookup['trim'] as $key => $value) { ?>
            <div class="trim">
                <label for="trim_style">
                    <img class="img-radio" alt="<?php echo $key; ?>" src="<?php echo $assetPath . '/'; ?>app/assets/images/trims/<?php echo $key; ?>.png" height="80px" /><br>
                    <div class="body-name"><?php echo $value; ?></div>
                    <input name="trim_style" type="radio" value="<?php echo $key ?>" <?php if ($check) echo 'checked'; ?> />
                    <?php $check = false; ?>
                </label>
            </div>
        <?php } ?>
        <!-- END FOREACH -->
    </div>
</div>
