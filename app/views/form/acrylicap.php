<div class="col-xs-8">
  <h4>Acrylicap<sup>&reg;</sup></h4>
  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="louver" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/louver.png" height="70px" />
          <span class="body-name">Louver</span><br>
          <input name="body_style" type="radio" value="louver" checked />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="horizontal_rib" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/horizontal_rib.png" height="70px" />
          <span class="body-name">Horizontal Rib</span><br>
          <input name="body_style" type="radio" value="horizontal_rib" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="brick" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/brick.png" height="70px" />
          <span class="body-name">Brick</span><br>
          <input name="body_style" type="radio" value="brick" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="72_rib" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/72_rib.png" height="70px" />
          <span class="body-name">72 Rib</span><br>
          <input name="body_style" type="radio" value="72_rib" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="pan" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/pan.png" height="70px" />
          <span class="body-name">Pan</span><br>
          <input name="body_style" type="radio" value="pan" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="forrest" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/forrest.png" height="70px" />
          <span class="body-name">Forrest</span><br>
          <input name="body_style" type="radio" value="forrest" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="batten" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/batten.png" height="70px" />
          <span class="body-name">Batten</span><br>
          <input name="body_style" type="radio" value="batten" />
      </label>
  </div>

  <div class="cap">
      <label for="body_style">
          <img class="img-radio" alt="vertical_rib" src="<?php echo $assetPath . '/'; ?>app/assets/images/styles/vertical_rib.png" height="70px" />
          <span class="body-name">Vertical Rib</span><br>
          <input name="body_style" type="radio" value="vertical_rib" />
      </label>
  </div>
</div>
