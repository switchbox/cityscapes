<div class="progress-container">
  <div class="row">
    <div class="bar"></div>
      <div class="col-sm-4">
        <div class="numberCircle on">1</div>
        <div class="progress-text">
            <span class="brand-green"><strong>STEP 1 - PANEL</strong></span><br>
            <span>
                Choose from industry standard<br>
                options or create your own<br>
                Made to Order design.
            </span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="numberCircle">2</div>
        <div class="progress-text">
            <span class="brand-green"><strong>STEP 2 - TRIM CAP</strong></span><br>
            <span>
                Choose from industry standard<br>
                options or create your own<br>
                Made to Order design.
            </span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="numberCircle">3</div>
        <div class="progress-text">
            <span class="brand-green"><strong>STEP 3 - SAVE and SHARE</strong></span><br>
            <span>
                Choose from industry standard<br>
                options or create your own<br>
                Made to Order design.
            </span>
        </div>
    </div>
</div>
